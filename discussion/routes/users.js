// [SECTION] Dependecies and Modules
const express = require('express');
const controller = require('../controllers/users.js');
const route = express.Router();

// [SECTION] Route
	// Create User
	route.post('/register', (req, res) => {
		let userInfo = req.body;
		controller.registerUser(userInfo).then((result) => res.send(result));
	})

	// Retrieve All Users
	route.get('/', (req, res) => {
		controller.getAllUsers().then((result) => res.send(result));
	})

	// Retrieve Single User
	route.get('/:id', (req, res) => {
		let userId = req.params.id;
		controller.getProfile(userId).then(result => res.send(result));
	})

	// Delete a user
	route.delete('/:id', (req, res) => {
		let userId = req.params.id;
		controller.deleteUser(userId).then(result => res.send(result));
	})

	// Update a user
	route.put('/:id', (req, res) => {
		let id = req.params.id;
		let body = req.body
		controller.updateUser(id, body).then(result => res.send(result));

	})



module.exports = route;