// [SECTION] Dependencies and Modules
	const express = require('express');
	const controller = require('../controllers/tasks.js')

// [SECTION] Routing Component
	route = express.Router();

// [SECTION] Tasks Routes
	// Create Task
		route.post('/', (req, res) => {
			let taskInfo = req.body
			controller.createTask(taskInfo).then((result) => res.send(result));
	})

	// Retrieve All Task
		route.get('/', (req, res) => {
			controller.getAllTasks().then((result) => res.send(result));
		})

	// Retrieve Single Task
		route.get('/:id', (req, res) => {
			let taskId = req.params.id;
			controller.getTask(taskId).then((result) => res.send(result));
		})

	// Delete a task
		route.delete('/:id', (req, res) => {
			let taskId = req.params.id;
			controller.deleteTask(taskId).then((result) => res.send(result));
		})

	// Update a task
		route.put('/:id', (req, res) => {
			let taskId = req.params.id;
			let status = req.body;
			controller.updateTask(taskId, status).then(result => res.send(result));
		})

// [SECTION] Expose Route System
	module.exports = route;

