// [SECTION] Dependecies and Modules
	const User = require('../models/User.js');
	const bcrypt = require('bcrypt')

// [SECTION] User Functionalities
	// Create User
		module.exports.registerUser = (reqBody) => {

			let fName = reqBody.firstName;
			let lName = reqBody.lastName;
			let email = reqBody.email;
			let password = reqBody.password;

			let newUser = new User({
				firstName: fName,
				lastName: lName,
				email: email,
				password: bcrypt.hashSync(password, 10)
			})
			return newUser.save().then((save, error) => {
				if (save) {
					return 'User has been created!';
				} 
				else {
					return 'Error in saving user!';
				}
			})
			
		}

	// Retrieve All Users
		module.exports.getAllUsers = () => {
			return User.find({}).then((result) => {
				return result;
			})
		}
	
	// Retrieve a single user
		module.exports.getProfile = (data) => {
			return User.findById(data).then(result => {
				
				result.password = '';
				return result;
			})
		} 

	// Delete a user
		module.exports.deleteUser = (userId) => {
			return User.findByIdAndRemove(userId).then((deleteUser, error) => {
				if (deleteUser) {
					return `Account Deleted Successfully`;
				} 
				else {
					return 'No Account were Removed';
				}
			})
		}

	// Update a user
		module.exports.updateUser = (userId, newContent) => {

			let fName = newContent.firstName
			let lName = newContent.lastName
			return User.findById(userId).then((foundUser, error) => {
				if (foundUser) {
					foundUser.firstName = fName;
					foundUser.lastName = lName;
					return foundUser.save().then((updatedUser, error) => {
						if (updatedUser) {
							return updatedUser;
						} 
						else {
							return false;
						}
					});
				} 
				else {
					return 'No user found'
				}
			})
		}