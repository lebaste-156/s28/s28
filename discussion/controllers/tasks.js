// [SECTION] Dependencies and Modules
	const Task = require('../models/Task.js')


// [SECTION] Functionalities
	// Create Task
		module.exports.createTask = (reqBody) => {

			let taskName = reqBody.name;
			let newTask = 	new Task({
			name: taskName
			})
			return newTask.save().then((save, error) => {
				if (save) {
					return 'Task has been saved!'
				} 
				else {
					return 'Saving New Task Failed!'
				}
			})
		}

	// Retrieve All Tasks
		module.exports.getAllTasks = () => {
			return Task.find({}).then((searchResult) => {
				return searchResult;
			})
		}

	// Retrieve Single Task
		module.exports.getTask = (taskId) => {
			return Task.findById(taskId).then(result => {
				return result
			})
		}

	// Delete a task
		module.exports.deleteTask = (taskId) => {
			return Task.findByIdAndRemove(taskId).then((deletedTask, error) => {
				if (deletedTask) {
					return 'Task has been deleted';
				} 
				else {
					return 'Error in deleting task';
				}
			})
		}

	// Update a task
		module.exports.updateTask = (taskId, newContent) => {
			let status = newContent.status
			return Task.findById(taskId).then((foundTask, error) => {
				if (foundTask) {
					foundTask.status = status;
					return foundTask.save().then((updatedTask, error) => {
						if (updatedTask) {
							return updatedTask;
						} 
						else {
							return false
						}
					})
				} 
				else {
					return 'No task found'
				}
			})
		}


