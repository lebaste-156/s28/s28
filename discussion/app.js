// [SECTION] Depedencies and Modules
	const express = require('express');
	const mongoose = require('mongoose');
	const dotenv = require('dotenv');
	const taskRoutes = require('./routes/tasks.js')
	const userRoutes = require('./routes/users.js')

// [SECTION] Environment Variables
	dotenv.config();
	let secret = process.env.CONNECTION_STRING
	
// [SECTION] Server Setup
	const server = express();
	server.use(express.json());
	const port = process.env.PORT;

// [SECTION] Database Connection
	mongoose.connect(secret);
	const db = mongoose.connection;
	db.on('open', () => console.log(`Connected to MongoDB`));
	db.once('error', () => console.log(`Connection error`))

// [SECTION] Routes	
	server.use('/tasks', taskRoutes);
	server.use('/users', userRoutes)


// [SECTION] Server Response
	server.listen(port, () => {
		console.log(`Server is running on ${port}`)
})

	server.get('/', (req, res) => {
		res.send('WELCOME!')
	})